import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle

r1 = 0.4292
r2 = 1.4292
r1s = 1.2083
r2s = 2.2083
nrows = 80

def plot_primitive_material_concentration(tracer_directory):

	conc_primitive_inside = []
	conc_primitive_outside = []
	pile_mass = []
	files = ['100','200','300','400','500','600','700','800','900']
	times = [0.0,0.5,1,1.5,2,2.5,3,3.5,4,4.5]

	for i in range(len(files)):

		df = pd.read_csv(str(tracer_directory)+'/tracer_history.'+files[i], delim_whitespace=True, header=None, engine='python')
		pools = pd.read_csv(str(tracer_directory)+'/pools.'+files[i], delim_whitespace=True, header=None)
		boxes = pd.read_csv(str(tracer_directory)+'/box_info.csv', delim_whitespace=True, header=None)

		pools.columns = ['x','y']
		r = np.sqrt(pools['x']**2 + pools['y']**2)
		number_of_pile_boxes_in_row = np.unique(r.round(decimals=4), return_counts=True)
		boxes = boxes.drop(boxes.columns[[0, 1]], axis=1)
		boxes.columns=['r', 'nboxes', 'theta', 'r*theta', 'nboxes']

		redges = np.linspace(r1s, r2s, len(boxes.index))
		thetas = np.asarray(boxes['theta'])

		box_areas = [0.5*(redges[j+1]**2 - redges[j]**2)*(thetas[j]) for j in range(len(redges) -1)]
		pile_area = [number_of_pile_boxes_in_row[1][i]*box_areas[i] for i in range(len(number_of_pile_boxes_in_row[1]))]

		df = df[2:].dropna(axis=1)
		df = df.astype('float64')
		df.columns = ['x', 'y', 'tsm', 'melt', 'pool']
		df = df.reset_index(drop=True)

		basalt = df.iloc[::2]
		Hz = df.iloc[1::2]

		basalt_particles_in_pool = np.count_nonzero(basalt['pool'])
		Hz_particles_in_pool = np.count_nonzero(Hz['pool'])

		primitive_basalt_particles_in_pool = basalt[(basalt['pool'] > 0.5) & (basalt['melt'] < 0.5)]
		primitive_Hz_particles_in_pool = Hz[(Hz['pool'] > 0.5) & (Hz['melt'] < 0.5)]

		primitive_basalt_particles_out_pool = basalt[(basalt['pool'] < 0.5) & (basalt['melt'] < 0.5)]
		primitive_Hz_particles_out_pool = Hz[(Hz['pool'] < 0.5) & (Hz['melt'] < 0.5)]

		domain_area = np.pi*(r2s**2 - r1s**2)
		mantle_mass = 4e24
		total_pile_area = sum(pile_area)
		basalt_particle_area = domain_area*0.125/len(basalt)
		Hz_particle_area = domain_area*0.875/len(Hz)
		basalt_particle_mass = mantle_mass*0.125/len(basalt)
		Hz_particle_mass = mantle_mass*0.875/len(Hz)
		domain_outside_pile_area = domain_area - total_pile_area

		pile_mass.append(basalt_particles_in_pool*basalt_particle_mass + Hz_particles_in_pool*Hz_particle_mass)

		if total_pile_area > domain_area*0.01:
			conc_primitive_inside.append((len(primitive_basalt_particles_in_pool.index)*basalt_particle_area + 
									len(primitive_Hz_particles_in_pool.index)*Hz_particle_area)/total_pile_area * 100)
		conc_primitive_outside.append((len(primitive_basalt_particles_out_pool.index)*basalt_particle_area + 
								len(primitive_Hz_particles_out_pool.index)*Hz_particle_area)/domain_outside_pile_area * 100)

		# print('number_in:', len(primitive_basalt_particles_in_pool.index))
		# print('number_out:', len(primitive_basalt_particles_out_pool.index))
		# print('conc_primitive_outside:', conc_primitive_outside)
		# print('conc_primitive_inside:', conc_primitive_inside)

	#conc_primitive_inside.insert(0,0.0)
	conc_primitive_outside.insert(0,100)
	pile_mass.insert(0,0.0)

	plt.figure(1)
	plt.plot(times, pile_mass)
	plt.grid()
	plt.ylabel('mass of piles (kg)')
	plt.xlabel('Time (Gyr)')
	plt.savefig('plots/pile_mass_1p_threshtest.png')

	plt.figure(2)
	times_len = len(times) - len(conc_primitive_inside)
	plt.plot(times[times_len:], conc_primitive_inside, c=(0.90196,0.62353,0.0))
	plt.plot(times, conc_primitive_outside, c=(0.0,0.56471,0.69804))
	plt.grid()
	plt.xlabel('Time (Gyr)')
	plt.ylabel('Concentration of primitive material (%)')                                                                                                                          
	handles = [Rectangle((0,0),1,1,color=c,ec="w") for c in
	           [(0.0,0.56471,0.69804),(0.90196,0.62353,0.0)]]
	labels= ["Ambient mantle","Thermochemical pile"]
	plt.legend(handles, labels)                                                                                                                                                     
	plt.savefig('plots/conc_primitive_particles_bb08_sph.png',dpi=300)

def plot_piles(tracer_directory):

	df = pd.read_csv(str(tracer_directory)+'/tracer_history.900', delim_whitespace=True, header=None, engine='python')
	pools = pd.read_csv(str(tracer_directory)+'/pools.900', delim_whitespace=True, header=None)

	df = df[2:].dropna(axis=1)
	df = df.astype('float64')
	df.columns = ['x', 'y', 'tsm', 'melt', 'pool']
	df = df.reset_index(drop=True)

	basalt = df.iloc[::2]
	basalt_down_sample = basalt.iloc[::10]
	Hz = df.iloc[1::2]

	basalt_particles_in_pool = basalt_down_sample[(basalt_down_sample['pool'] > 0.5)]

	plt.scatter(basalt_down_sample['x'],basalt_down_sample['y'],c='grey',s=0.5,edgecolors='grey',linewidth='0.0')
	plt.scatter(basalt_particles_in_pool['x'],basalt_particles_in_pool['y'],c='red',s=0.5,edgecolors='red',linewidth='0.0')
	plt.savefig('plots/particle_plot.png',dpi=300)


if __name__ == "__main__":
	plot_primitive_material_concentration('../dln64_thresh_test')
	#plot_piles('../dln64_thresh_test')





